#include <date/date.h>

#include <bonseyes/json.hpp>

namespace nviso {

namespace C = std::chrono;

void to_json(json& j, const C::system_clock::time_point& p) {
  auto dp = date::floor<date::days>(p);
  auto ymd = date::year_month_day{dp};
  auto time = date::make_time(C::duration_cast<C::microseconds>(p - dp));

  j = json{{"y", (int)ymd.year()}, {"m", (unsigned)ymd.month()}, {"d", (unsigned)ymd.day()},
  {"h", time.hours().count()}, {"min", time.minutes().count()},
  {"s", time.seconds().count() + time.subseconds().count() * 1.0e-6}};
}

void from_json(const json& j, const C::system_clock::time_point& p) {
 // FIXME
}

} // nviso


